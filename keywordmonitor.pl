#! /usr/bin/env perl

use strict;
use warnings;
use Encode;
use Data::Dumper;
use XML::FeedPP;
use File::Copy;
use File::Spec;
use diagnostics;

sub mailsend
{
	my($from, $to, $subject, $msg) = @_;

	my $sendmail = '/usr/sbin/sendmail'; # sendmailコマンドパス
	open(SDML,"| $sendmail -t -i") || die 'sendmail error';
	# メールヘッダ出力
	print SDML "From: $from\n";
	print SDML "To: $to\n";
	print SDML "Subject: $subject\n";
	print SDML "Content-Transfer-Encoding: 7bit\n";
	print SDML "Content-Type: text/plain;\n\n";
	#メール本文出力
	print SDML "$msg";
	# sendmail コマンド閉じる
	close(SDML);
}

sub alreadyexist
{
	my ($data, $firstline) = @_;
	if(!$data || !$firstline){
		return 0;
	}
	#print length($data).':'.length($firstline);
	if($firstline eq $data){
		#print 'same\n';
		return 1;
	}
	return 0;
}

my $from = 'news@junkpiano.me'; # 送信元メールアドレス
my $to = 'yusukeoha@gmail.com'; # あて先メールアドレス
my $subject = 'news headlines'; # メールの件名
my $home = $ENV{'HOME'};

my $feed = XML::FeedPP->new( 'http://feeds.pinboard.in/rss/t:yapcasia' );

my $dir = $ENV{'HOME'}."/src/tfpmonitor/";

my $firstdata;
open(RH, '<', $dir."l.txt") or die "die at open l.txt $!";
while(my $line = <RH>) {
	$firstdata = $line;
	last;
}
close(RH);

open(FH, '>', $dir."l.txt") or die "die at l.txt $!";

my $countup=0;
my $msg;

foreach my $item ($feed->get_item()) {
	my $data = 'URL: '.$item->link()."\n";
	my $exist = alreadyexist($data, $firstdata);
	if($exist == 1){
		print FH $firstdata;
		last;
	}
	$msg .= $data;
	$msg .= 'Title: '. $item->title(). "\n";
	if($item->description()){
		$msg .= 'DESC: '. $item->description(). "\n";
	}
	$msg .= '----------'. "\n";
	if($countup == 0){
		print FH $msg;
	}
	$countup++;
}

if($msg && length($msg) > 0){
	mailsend($from, $to, $subject, $msg);
}

close(FH);


